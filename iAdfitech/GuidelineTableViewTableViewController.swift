//
//  GuidelineTableViewTableViewController.swift
//  iAdfitech
//
//  Created by Ryan Parker on 4/19/15.
//  Copyright (c) 2015 Tim Zuercher and Ryan Parker. All rights reserved.
//

import UIKit
import SwiftyJSON
class GuidelineTableViewTableViewController: UITableViewController {
    
    //Variables to hold the data that is recieved from the API
    var investor :String?=""
    var client :String?=""
    var program :String?=""
    var search_text :String?=""
    var date :String?=""
    var current_page :Int=1
    var total_pages :Int?=1
    
    //Function that reloads the table with Search results when returning from Search modal view
    @IBAction func unwindToViewVC (unwindSegue: UIStoryboardSegue) {
        var searchVC = unwindSegue.sourceViewController as? SearchViewController
        investor = searchVC?.investorField.text
        program = searchVC?.programField.text
        client = searchVC?.clientField.text
        search_text = searchVC?.fullText.text
        date = searchVC?.dateTextField.text
        reloadTable()
    }
    
    //Action that reloads the table when returning from the DetailViewController
    @IBAction func unwindToViewVCCancel (unwindSegue: UIStoryboardSegue) {}
    
    func reloadTable(){
        Guideline.search(investor: investor, program: program, client: client,search_text: search_text ,date: date,page : current_page, completion:{ (response,page_size) -> () in
            self.guidelines=response
            self.total_pages=page_size
            dispatch_async(dispatch_get_main_queue(),
                {
                    self.tableView.reloadData()
                }
            )
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        var refresher = UIRefreshControl()
        refresher.addTarget(self, action: "handleRefresh:", forControlEvents: UIControlEvents.ValueChanged)
        refreshControl = refresher
        reloadTable()
        
        
    }
    
    //Handles load to refresh
    @IBAction func handleRefresh(sender: AnyObject?) {
        reloadTable()
        refreshControl!.endRefreshing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Prepares a data object to be sent when segueing to the Detail View
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let guideDetailsVC = segue.destinationViewController as? GuidelineDetailViewController {
            if segue.identifier == "showGuidelineDetail" {
                let row = tableView!.indexPathForSelectedRow()!.row
                let guideline = guidelines[row] as Guideline
                guideDetailsVC.guideline = guideline
            }
        }
    }

    // MARK: - Table view data source
    
    var guidelines : Array <Guideline> = []
    
     override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return guidelines.count
    }
    
    //Populates the TableView with the data returned from the API
    override func tableView (_tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("GuidelineCell") as! GuidelineCellTableViewCell
        
        let guideline = guidelines[indexPath.row]
        
        cell.guideTitle!.text = guideline.title
        //Check for Investor
        if(is_blank(guideline.investor))
        {
            cell.guideInvestor!.text = "None"
        }else
        {
            cell.guideInvestor!.text = guideline.investor
        }
        //Check for Client
        if(is_blank(guideline.client))
        {
            cell.guideClient!.text = "None"
        }else
        {
            cell.guideClient!.text = guideline.investor
        }
        //Check for Program
        if(is_blank(guideline.program))
        {
            cell.guideProgram!.text = "None"
        }else
        {
            cell.guideProgram!.text = guideline.investor
        }
        //Check for Start Date
        if(is_blank(guideline.start_date))
        {
            cell.sDate!.text = "None"
        }else
        {
            cell.sDate!.text = guideline.start_date
        }
        //Check for End Date
        if(is_blank(guideline.end_date))
        {
            cell.eDate!.text = "None"
        }else
        {
            cell.eDate!.text = guideline.end_date
        }
        
        if(indexPath.row == self.guidelines.count - 1)
        {
            //todo
//            if(current_page != total_pages){
//            current_page+=1
//                reloadTable()
//            
//            }
        }
        
        return cell
    }
    
    //Checks for blank string
    func is_blank(str : String?) ->Bool{
        if(str != nil){
            if(str != ""){
                return false
            }
        }
        return true
    }
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
