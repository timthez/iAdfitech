//
//  GuidelineDetailViewController.swift
//  iAdfitech
//
//  Created by Ryan Parker on 4/9/15.
//  Copyright (c) 2015 Tim Zuercher and Ryan Parker. All rights reserved.
//

import UIKit

class GuidelineDetailViewController: UIViewController {
    
    //Detail View elements
    @IBOutlet weak var detailTitle: UILabel!
    @IBOutlet weak var detailInvestor: UILabel!
    @IBOutlet weak var detailClient: UILabel!
    @IBOutlet weak var detailProgram: UILabel!
    @IBOutlet weak var detailBody: UITextView!
    
    var guideline : Guideline = Guideline()
    
    //Load the passed guideline object into the Detail View elements
    func loadDetails(){
        detailTitle.text = guideline.title
        //Check for Investor
        if(is_blank(guideline.investor))
        {
            detailInvestor.text = "None"
        }else
        {
            detailInvestor.text = guideline.investor
        }
        //Check for Client
        if(is_blank(guideline.client))
        {
            detailClient.text = "None"
        }else
        {
            detailClient.text = guideline.investor
        }
        //Check for Program
        if(is_blank(guideline.program))
        {
            detailProgram.text = "None"
        }else
        {
            detailProgram.text = guideline.investor
        }
        //Check for Body
        if(is_blank(guideline.body))
        {
            detailBody.text = "None"
        }else
        {
            var markdown = Markdown()
            let informationData = markdown.transform(guideline.body!).dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
            let informationString = NSAttributedString(data: informationData!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil, error: nil)
            detailBody.attributedText = informationString
        }
    }
    
    //Checks for blank string
    func is_blank(str : String?) ->Bool{
        if(str != nil){
            if(str != ""){
                return false
            }
        }
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadDetails()
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
