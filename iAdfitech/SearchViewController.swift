//
//  SearchViewController.swift
//  iAdfitech
//
//  Created by Ryan Parker on 4/7/15.
//  Copyright (c) 2015 Tim Zuercher and Ryan Parker. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITextViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Search View Modal elements
    @IBOutlet weak var fullText: UITextField!
    @IBOutlet weak var investorField: UITextField!
    @IBOutlet weak var clientField: UITextField!
    @IBOutlet weak var programField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var orderField: UITextField!
    
    //Loads the datepicker
    @IBAction func textUpdate(sender: UITextField) {
        var datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    //Update the datepicker value to the text box
    func datePickerValueChanged(sender: UIDatePicker) {
        var dateformatter = NSDateFormatter()
        dateformatter.dateStyle = NSDateFormatterStyle.MediumStyle
        dateTextField.text = dateformatter.stringFromDate(sender.date)
    }
    
    //Get rid of keyboard on tap of the background
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }
    

}
