//
//  GuidelinesModel.swift
//  iAdfitech
//
//  Created by Ryan Parker on 4/19/15.
//  Copyright (c) 2015 Tim Zuercher and Ryan Parker. All rights reserved.
//

import UIKit
//import Alamofire
//import SwiftyJSON

class Guideline: NSObject {
    
    //Variables that make up a guideline
    var investor : String?
    var client : String?
    var program : String?
    var parent : Int?
    var title : String?
    var body : String?
    var start_date : String?
    var end_date : String?
    
    override init(){
        super.init()
    }
    
    //Initializer
    init(investor : String?, client : String?, program : String?, parent : Int?, title : String?, body : String?, start_date : String?, end_date : String?) {
        super.init()
        self.investor = investor
        self.client = client
        self.program = program
        self.parent = parent
        self.title = title
        self.body = body
        self.start_date = start_date
        self.end_date = end_date
    }
    
    //Performs a search query on the API for specifed guidelines
    class func search(investor : String? = "", client : String? = "" , program : String? = "",date : String? = "", custom_order : String? = "custom_order", search_text : String? = "",page : Int? = 1 ,completion: (response: [Guideline],page_total: Int?) -> ()) {
        
        var guidelines :[Guideline] = []
        var search :[String : AnyObject] = [ "id" : 0, "investor" : investor!, "program" : program!, "client" : client!, "date" : date!, "custom_order" : custom_order!, "recursive" : true, "search" : search_text!, "state_filter" : 1 ]
        var page :[String :AnyObject] = ["page_size": 200, "page_number": page!, "page_count": ""]
        var parameters :[String :AnyObject] = ["search": search, "page": page]

        let response = request(.GET, "http://techknow-3.local:8080/guidelines/search", parameters: parameters)
        response.responseJSON{
            (request, response, json, error) in
            let data = JSON(json!)
            let total = data["guidelines"].count
            if (total != 0){
            for i in 0...(total-1){
                let t_investor = data["guidelines"][i]["investor"].string
                let t_client = data["guidelines"][i]["client"].string
                let t_program = data["guidelines"][i]["program"].string
                let t_parent = data["guidelines"][i]["parent"].int
                let t_title = data["guidelines"][i]["title"].string
                let t_body = data["guidelines"][i]["body"].string
                let t_start_date = data["guidelines"][i]["start_date"].string
                let t_end_date = data["guidelines"][i]["end_date"].string
                guidelines.append(Guideline(investor: t_investor, client: t_client ,program: t_program , parent: t_parent, title: t_title, body: t_body, start_date: t_start_date , end_date: t_end_date))
            }
            }
            completion(response: guidelines,page_total : data["page"]["page_count"].int)
        }
        
    }
    
}
