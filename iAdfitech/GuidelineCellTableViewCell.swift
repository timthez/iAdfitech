//
//  GuidelineCellTableViewCell.swift
//  iAdfitech
//
//  Created by Ryan Parker on 4/19/15.
//  Copyright (c) 2015 Tim Zuercher and Ryan Parker. All rights reserved.
//

import UIKit

class GuidelineCellTableViewCell: UITableViewCell {

    //TableView Cell element variables
    @IBOutlet weak var guideTitle: UILabel!
    @IBOutlet weak var guideInvestor: UILabel!
    @IBOutlet weak var guideClient: UILabel!
    @IBOutlet weak var guideProgram: UILabel!
    @IBOutlet weak var sDate: UILabel!
    @IBOutlet weak var eDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
